Le programme permet de créer un automate , puis lui soumettre une chaîne de caractères pour 
laquelle il est capable de dire s'il la reconnaît ou pas. 

le constructeur prend en paramètre le nombre d'états non acceptant et le nombre d'états acceptants

Il faut appeler la méthode addNoAcceptingState pour ajouter des états non acceptant. Elle prend 
en parmaètre l'ordre de l'état (premier, deuxième ..) et le label (1, 2, 3 ....)

Il en va de même pour la méthode  addAcceptingState qui va permettre de renseigner 
les états acceptants

La méthode show va permettre d'afficher l'automate ainsi construit : 
les états acceptants
les états non acceptants
les chemins entres les différents états et les valeurs qu'ils consomment

la méthode check va prendre en apramètre une chaîne de caractère et vérifier si elle est reconnut
par l'automate. Si elle est reconnut, l'automate renvoit true, sinon il renvoit false. Chaque réponse
est accompagnée d'un méssage dans le terminale