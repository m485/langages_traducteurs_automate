package automate;

import java.util.Arrays;

public class Automate {
	private int[] notAccepting;
	private int[] accepting;
	private char[][] path;
	
	public Automate(int noAccepting, int Accepting ) {
		this.notAccepting = new int[noAccepting];
		this.accepting = new int[Accepting];
		this.path = new char[noAccepting+Accepting][noAccepting+Accepting];
	}

	public void addNoAcceptingState(int position,int state) {
		this.notAccepting[position] = state;
	}
	
	public void addAcceptingState(int position,int state) {
		this.accepting[position] = state;
	}
	
	public void addPath(int a,int b,char value) {
		path[a][b] = value;
	}
	
	public void show() {
		System.out.println("notAccepting States: "+ Arrays.toString(this.notAccepting) );
		System.out.println("Accepting States: "+Arrays.toString(this.accepting));
		System.out.println("Path: ");
		for (int i = 0; i < path.length; i++) { 
			for (int j = 0; j < path[i].length; j++) {
				if(path[i][j] != ' ') {
					System.out.println(i +" - " + j + " = "+path[i][j]);
				} 
			}
		}
	}
	
	public boolean check(String chaine) {
		int currentState = 0;
		//check if first state is accepting
		if (contains(this.accepting,currentState)) {
			System.out.println("   | Accepté");
            return true;
		}
		char[] parts = new char[chaine.length( )];
		int compteur = 0;
		//get all characters in the chaine
		for (int i=0; i < chaine.length(); i++) {
		    parts[i] = chaine.charAt(i);
		}
		
		for (int i = 0; i < parts.length; i++){
			System.out.print(parts[i]+" ");
			compteur += 1;
			char currentChar = parts[i];
			for (int j = 0; j < path[currentState].length; j++) {
				//check the path from currentState 
				if(path[currentState][j] == currentChar) {
					//chack if next state is an accepting state
					if(contains(this.accepting, j) && compteur == chaine.length()){
			            System.out.println("   | Accepté");
			            return true;
			        }
					//update for next state
					currentState = j;
					
					break;
				}
			}
		}
		System.out.println("   | Refusé");
		return false;
	}
	
	public static boolean contains(final int[] array, final int v) {
        boolean result = false;
        for(int i : array){
            if(i == v){
                result = true;
                break;
            }
        }
        return result;
    }
}
