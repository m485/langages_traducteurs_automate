package automate;

public class Main {

	public static void main(String[] args) {
		//System.out.println("hello");
		
		Automate a = new Automate(4,1);

		a.addNoAcceptingState(0,0);
		a.addNoAcceptingState(1,1);
		a.addNoAcceptingState(2,2);
		a.addNoAcceptingState(3,3);
		
		a.addAcceptingState(0,4);
		
		
		a.addPath(0, 1, 'p');
		a.addPath(0, 2, 'a');
		a.addPath(0, 4, 'z');
		a.addPath(1, 3, 'i');
		a.addPath(1, 2, 'a');
		a.addPath(2, 3, 'p');
		a.addPath(3, 4, 'a');
		
		a.show();
		
		System.out.println("=========================================");
		
		a.check("papa");
		a.check("pia");
		a.check("papi");

	}

}